package com.snyamathi.android.bluetoothinput;

import java.io.IOException;
import java.util.ArrayList;

import com.stericson.RootTools.Command;
import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.Shell;

public class InputFairy {

	private ArrayList<MyCommand> buttonAdown;
	private ArrayList<MyCommand> buttonAup;
	private ArrayList<MyCommand> buttonBdown;
	private ArrayList<MyCommand> buttonBup;
	private ArrayList<MyCommand> buttonCdown;
	private ArrayList<MyCommand> buttonCup;
	private Shell shell;

	public InputFairy() {
		buttonAdown = new ArrayList<MyCommand>();
		buttonAdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0048 00000070"}));
		buttonAdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0050 00000010"}));
		buttonAdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0053 00000344"}));
		buttonAdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0054 00000892"}));
		buttonAdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0000 0002 00000000"}));
		buttonAdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0020 00000001"}));
		buttonAdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0000 0000 00000000"}));
		buttonAdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0048 00000070"}));
		buttonAdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0050 00000010"}));
		buttonAdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0053 00000344"}));
		buttonAdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0054 00000892"}));
		buttonAdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0000 0002 00000000"}));
		buttonAdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0020 00000001"}));
		buttonAdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0000 0000 00000000"}));

		buttonAup = new ArrayList<MyCommand>();
		buttonAup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0048 00000000"}));
		buttonAup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0050 00000010"}));
		buttonAup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0053 00000344"}));
		buttonAup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0054 00000892"}));
		buttonAup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0000 0002 00000000"}));
		buttonAup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0020 00000000"}));
		buttonAup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0000 0000 00000000"}));
		buttonAup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0048 00000000"}));
		buttonAup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0050 00000010"}));
		buttonAup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0053 00000344"}));
		buttonAup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0054 00000892"}));
		buttonAup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0000 0002 00000000"}));
		buttonAup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0020 00000000"}));
		buttonAup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0000 0000 00000000"}));

		buttonBdown = new ArrayList<MyCommand>();
		buttonBdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0048 00000070"}));
		buttonBdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0050 00000010"}));
		buttonBdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0053 00000148"}));
		buttonBdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0054 00000853"}));
		buttonBdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0000 0002 00000000"}));
		buttonBdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0020 00000001"}));
		buttonBdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0000 0000 00000000"}));
		buttonBdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0048 00000070"}));
		buttonBdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0050 00000010"}));
		buttonBdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0053 00000148"}));
		buttonBdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0054 00000853"}));
		buttonBdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0000 0002 00000000"}));
		buttonBdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0020 00000001"}));
		buttonBdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0000 0000 00000000"}));

		buttonBup = new ArrayList<MyCommand>();
		buttonBup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0048 00000000"}));
		buttonBup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0050 00000010"}));
		buttonBup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0053 00000148"}));
		buttonBup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0054 00000853"}));
		buttonBup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0000 0002 00000000"}));
		buttonBup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0020 00000000"}));
		buttonBup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0000 0000 00000000"}));
		buttonBup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0048 00000000"}));
		buttonBup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0050 00000010"}));
		buttonBup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0053 00000148"}));
		buttonBup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0054 00000853"}));
		buttonBup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0000 0002 00000000"}));
		buttonBup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0020 00000000"}));
		buttonBup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0000 0000 00000000"}));

		buttonCdown = new ArrayList<MyCommand>();
		buttonCdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0048 00000070"}));
		buttonCdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0050 00000010"}));
		buttonCdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0053 00000198"}));
		buttonCdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0054 00000133"}));
		buttonCdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0000 0002 00000000"}));
		buttonCdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0020 00000001"}));
		buttonCdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0000 0000 00000000"}));
		buttonCdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0048 00000070"}));
		buttonCdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0050 00000010"}));
		buttonCdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0053 00000198"}));
		buttonCdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0054 00000133"}));
		buttonCdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0000 0002 00000000"}));
		buttonCdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0003 0020 00000001"}));
		buttonCdown.add(new MyCommand(0, new String[] {"sendevent /dev/input/event3 0000 0000 00000000"}));

		buttonCup = new ArrayList<MyCommand>();
		buttonCup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0048 00000000"}));
		buttonCup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0050 00000010"}));
		buttonCup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0053 00000198"}));
		buttonCup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0054 00000133"}));
		buttonCup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0000 0002 00000000"}));
		buttonCup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0020 00000000"}));
		buttonCup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0000 0000 00000000"}));
		buttonCup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0048 00000000"}));
		buttonCup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0050 00000010"}));
		buttonCup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0053 00000198"}));
		buttonCup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0054 00000133"}));
		buttonCup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0000 0002 00000000"}));
		buttonCup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0003 0020 00000000"}));
		buttonCup.add(new MyCommand(4, new String[] {"sendevent /dev/input/event3 0000 0000 00000000"}));

		try {
			shell = RootTools.getShell(true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendClick(ArrayList<MyCommand> commands) {
		try {
			for (MyCommand c : commands) {
				shell.runRootCommand(c);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public class MyCommand extends Command {

		public MyCommand(int id, String[] command) {
			super(id, command);
		}

		@Override
		public void output(int arg0, String arg1) {
			// TODO Auto-generated method stub
		}

	}

	public void sendInput(int read) {
		System.out.println(read);
		switch (read) {
		case 48:
			sendClick(buttonAdown);
			break;
		case 49:
			sendClick(buttonAup);
			break;
		case 50:
			sendClick(buttonBdown);
			break;
		case 51:
			sendClick(buttonBup);
			break;
		case 52:
			sendClick(buttonCdown);
			break;
		case 53:
			sendClick(buttonCup);
			break;
		}

	}	
}

const int buttonPinA =  8;
const int buttonPinB = 12;
const int ledPin     = 13;
const int potPin     = A0;

boolean buttonA = false;
boolean buttonB = false;
boolean buttonC = false;

void setup() {
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);  
  pinMode(buttonPinA, INPUT);  
  pinMode(buttonPinB, INPUT);  
}

void loop() {
  if (!buttonA && digitalRead(buttonPinA) == HIGH) {
    buttonA = true;
    Serial.print(0);
  } 
  else if (buttonA && digitalRead(buttonPinA) == LOW) {
    buttonA = false;
    Serial.print(1);
  } 

  if (!buttonB && digitalRead(buttonPinB) == HIGH) {
    buttonB = true;
    Serial.print(2);
  } 
  else if (buttonB && digitalRead(buttonPinB) == LOW) {
    buttonB = false;
    Serial.print(3);
  } 

  if (!buttonC && analogRead(potPin) > 500) {
    buttonC = true;
    Serial.print(4);
  } 
  else if (buttonC && analogRead(potPin) < 500) {
    buttonC = false;
    Serial.print(5);
  }
}



